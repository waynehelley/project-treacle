﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookTowardsCursor : MonoBehaviour {
    
    void Update()
    {
        //if (Input.GetMouseButton(0))
            Turn();
    }

    void Turn()
    {
        var mousePos = Input.mousePosition;
        mousePos.z = 10.0f;
        Vector3 lookPosition = Camera.main.ScreenToWorldPoint(mousePos);
        lookPosition = lookPosition - transform.position;
        float rotAngle = Mathf.Atan2(lookPosition.y, lookPosition.x) * Mathf.Rad2Deg -90;
        transform.rotation = Quaternion.AngleAxis(rotAngle, Vector3.forward);
    }

}
