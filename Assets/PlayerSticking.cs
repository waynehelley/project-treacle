﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSticking : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {        
        if (collision.gameObject.tag=="Player")
        {
            Debug.Log("Player Collision");
            Rigidbody2D rb2d = collision.gameObject.GetComponent<Rigidbody2D>();
            rb2d.isKinematic = true;
            PropelPlayer ppScript = collision.gameObject.GetComponent<PropelPlayer>();
            ppScript.playerGrounded = true;
            rb2d.velocity = new Vector2(0, 0);
        }
    }
}
