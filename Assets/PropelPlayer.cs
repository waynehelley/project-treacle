﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropelPlayer : MonoBehaviour {

    public bool playerGrounded = true;
    private Rigidbody2D rb;
    private GameObject arrow;
    public float arrowAngle;
    private float force = 1000;
    public float forceX;
    public float forceY;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        arrow = GameObject.FindGameObjectWithTag("Arrow");
	}
	
	// Update is called once per frame
	void Update () {

        arrowAngle = arrow.transform.rotation.eulerAngles.z;

        forceX = - force * Mathf.Sin(Mathf.Deg2Rad*arrowAngle);
        forceY = force * Mathf.Cos(Mathf.Deg2Rad*arrowAngle);

        if (Input.GetMouseButton(0) && playerGrounded)
        {
            rb.isKinematic = false;
            rb.AddForce(new Vector2(forceX, forceY));
            playerGrounded = false;
        }

    }
}
