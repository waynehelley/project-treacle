﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorYAxis : MonoBehaviour {

    public float speed = 100;

	// Update is called once per frame
	void Update () {

        // Rotate the object around its local X axis at 1 degree per second
        transform.Rotate(Vector3.up * Time.deltaTime * speed);
        
    }
}
